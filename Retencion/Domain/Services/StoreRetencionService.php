<?php

namespace App\Modules\Accounting\Retencion\Domain\Services;

use App\Modules\Accounting\Retencion\Domain\DTOs\RetencionStoreData;
use App\Modules\Accounting\Retencion\Domain\Models\Retencion;
use App\Modules\Support\Domain\Services\AuthService;
use Carbon\Carbon;

final class StoreRetencionService
{
    public function execute(RetencionStoreData $data)
    {
        $user = AuthService::user();
        $timestamp = Carbon::now($user->timeZone->descripcion);

        $retencion = new Retencion();
        $retencion->fecha = $data->fecha;
        $retencion->numero_recibo = $data->numeroRecibo;
        $retencion->nombre = $data->nombre;
        $retencion->forma_id = $data->formaRetencion->getValue();
        $retencion->tipo_id = $data->tipoRetencion->getValue();
        $retencion->importe_neto = $data->importeNeto;
        $retencion->porcentaje_it = $data->porcentajeIT;
        $retencion->origen_id = $data->origenId;
        $retencion->origen_detalle_id = $data->origenDetalleId;
        $retencion->origen_type = $data->origenType;
        $retencion->sucursal_id = $data->sucursalId;

        $retencion->user_id = $user->id;
        $retencion->empresa_id = $user->empresa_id;
        $retencion->created_at = $timestamp;
        $retencion->updated_at = $timestamp;

        $retencion->save();
    }
}
