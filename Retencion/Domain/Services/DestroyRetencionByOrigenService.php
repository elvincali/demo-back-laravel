<?php

namespace App\Modules\Accounting\Retencion\Domain\Services;

use App\Modules\Accounting\Retencion\Domain\Models\Retencion;

final class DestroyRetencionByOrigenService
{
    public function execute(string $origenType, int $origenId): void
    {
        Retencion::query()->deleteLogicFromOrigen($origenType, $origenId);
    }
}
