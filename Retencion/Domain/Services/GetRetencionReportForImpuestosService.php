<?php

namespace App\Modules\Accounting\Retencion\Domain\Services;

use App\Modules\Accounting\Retencion\Domain\Repositories\RetencionRepository;

final class GetRetencionReportForImpuestosService
{
    /**
     * @var RetencionRepository
     */
    private $repository;

    public function __construct(RetencionRepository $repository)
    {
        $this->repository = $repository;
    }

    public function execute(int $sucursalId, int $year, int $month)
    {
        return $this->repository->getReportForImpuestos($sucursalId, $year, $month);
    }
}
