<?php

namespace App\Modules\Accounting\Retencion\Domain\Models;

use App\Modules\Accounting\Retencion\Domain\QueryBuilders\RetencionQueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Retencion extends Model
{
    protected $table = 'retencion';

    /**
     * @return Builder|RetencionQueryBuilder
     */
    public static function query()
    {
        return parent::query();
    }

    public function newEloquentBuilder($query)
    {
        return new RetencionQueryBuilder($query);
    }
}
