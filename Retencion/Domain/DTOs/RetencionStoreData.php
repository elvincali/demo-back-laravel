<?php

namespace App\Modules\Accounting\Retencion\Domain\DTOs;

use App\Modules\Accounting\Retencion\Domain\Types\FormaRetencion;
use App\Modules\Accounting\Retencion\Domain\Types\TipoRetencion;
use Carbon\Carbon;

class RetencionStoreData
{
    /**
     * @var Carbon
     */
    public $fecha;

    /**
     * @var string|null
     */
    public $numeroRecibo;

    /**
     * @var string
     */
    public $nombre;

    /**
     * @var FormaRetencion
     */
    public $formaRetencion;

    /**
     * @var TipoRetencion
     */
    public $tipoRetencion;

    /**
     * @var float
     */
    public $importeNeto;

    /**
     * @var float
     */
    public $porcentajeIUE;

    /**
     * @var float
     */
    public $porcentajeIT;

    /**
     * @var float
     */
    public $porcentajeRCIVA;

    /**
     * @var int
     */
    public $origenId;

    /**
     * @var int|null
     */
    public $origenDetalleId;

    /**
     * @var string
     */
    public $origenType;

    /**
     * @var int
     */
    public $sucursalId;

    public static function fromOrigen(array $data): RetencionStoreData
    {
        $dto = new self();
        $dto->fecha = Carbon::parse($data['fecha']);
        $dto->numeroRecibo = $data['numeroRecibo'];
        $dto->nombre = $data['nombre'];
        // $dto->formaRetencion = FormaRetencion::from($data['formaRetencionId']);
        $dto->formaRetencion = new FormaRetencion($data['formaRetencionId']);
        // $dto->tipoRetencion = TipoRetencion::from($data['tipoRetencionId']);
        $dto->tipoRetencion = new TipoRetencion($data['tipoRetencionId']);
        $dto->importeNeto = $data['importeNeto'];
        $dto->porcentajeIUE = $data['porcentajeIUE'];
        $dto->porcentajeIT = $data['porcentajeIT'];
        $dto->porcentajeRCIVA = $data['porcentajeRCIVA'];
        $dto->origenId = $data['origenId'];
        $dto->origenDetalleId = array_key_exists('origenDetalleId', $data) ? $data['origenDetalleId'] : null;
        $dto->origenType = $data['origenType'];
        $dto->sucursalId = $data['sucursalId'];

        return $dto;
    }
}
