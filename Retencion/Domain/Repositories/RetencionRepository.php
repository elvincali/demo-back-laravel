<?php

namespace App\Modules\Accounting\Retencion\Domain\Repositories;

use App\Modules\Support\Domain\Services\AuthService;
use Illuminate\Support\Facades\DB;

final class RetencionRepository
{
    public function getByOrigen(string $origenType, int $origenId)
    {
        $sql = "select r.fecha, r.numero_recibo, r.nombre, r.forma_id, r.tipo_id, porcentaje_iue, porcentaje_it, porcentaje_rciva,
                origen_detalle_id
                from retencion r
                where r.origen_type = ?
                and r.origen_id = ?;";

        return DB::select($sql, [$origenType, $origenId]);
    }

    public function firstByOrigen(string $origenType, int $origenId)
    {
        $retenciones = $this->getByOrigen($origenType, $origenId);
        if (count($retenciones) == 0) return null;

        return $retenciones[0];
    }

    public function getTotalByOrigen(string $origenType, int $origenId)
    {
        $sql = "select sum(r.importe_neto * ((r.porcentaje_iue  + r.porcentaje_it + r.porcentaje_rciva) / 100)) as total_retencion
                from retencion r
                where r.origen_type = ?
                and r.origen_id = ?;";

        return DB::selectOne($sql, [$origenType, $origenId])->total_retencion;
    }

    public function getReportForImpuestos(int $sucursalId, int $year, int $month)
    {
        $user = AuthService::user();
        $whereSucursal = $sucursalId == 0 ? "" : "and r.sucursal_id = ${sucursalId}";

        $sql = "
            select t.*, (t.importe_neto - t.importe_iue125 - t.importe_iue5 - t.importe_it - t.importe_rciva) as importe_bruto
            from (
                select r.id, r.fecha, r.numero_recibo, r.nombre, tr.nombre tipo_retencion, r.importe_neto,
                round(if(r.tipo_id = 1, r.importe_neto * (r.porcentaje_iue / 100), 0), 2) as importe_iue125,
                round(if(r.tipo_id = 2, r.importe_neto * (r.porcentaje_iue / 100), 0), 2) as importe_iue5,
                round(if(r.tipo_id = 3 or r.tipo_id = 4, r.importe_neto * (r.porcentaje_rciva / 100), 0), 2) as importe_rciva,
                round(IF(r.tipo_id = 1 or r.tipo_id = 2 or r.tipo_id = 3, r.importe_neto * (r.porcentaje_it / 100), 0), 2) as importe_it,
                concat(r.origen_type, ':',
                case r.origen_type
                    when 'SD' then (select md.numero from movimiento_dinero md where md.id = r.origen_id limit 1)
                    when 'IP' then (select c.numero from compras c where c.id = r.origen_id limit 1)
                    when 'CXP' then (select cxp.numero from cuentas_por_pagar cxp where cxp.id = r.origen_id limit 1)
                    else 'N/E'
                end
                ) origen,
                case r.origen_type
                    when 'SD' then (select md.created_at from movimiento_dinero md where md.id = r.origen_id limit 1)
                    when 'IP' then (select c.created_at from compras c where c.id = r.origen_id limit 1)
                    when 'CXP' then (select cxp.created_at from cuentas_por_pagar cxp where cxp.id = r.origen_id limit 1)
                end as created_at
                from retencion r
                join tipo_retencion tr on r.tipo_id = tr.id
                where year(r.fecha) = ?
                and month(r.fecha) = ?
                ${whereSucursal}
                and r.empresa_id = ?
                and r.estado = 1

                ) t
            order by t.fecha desc, t.created_at desc;";

        return DB::select($sql, [$year, $month, $user->empresa_id]);
    }

    public function getTiposRetencion()
    {
        $sql = "select id, nombre
                from tipo_retencion
                where estado = 1;";

        return DB::select($sql);
    }
}
