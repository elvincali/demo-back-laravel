<?php

namespace App\Modules\Accounting\Retencion\Domain\Types;

use MyCLabs\Enum\Enum;

final class TipoRetencion extends Enum
{
    private const SERVICIOS = 1;

    private const BIENES = 2;

    private const ALQUILER = 3;

    private const VIATICOS = 4;
}
