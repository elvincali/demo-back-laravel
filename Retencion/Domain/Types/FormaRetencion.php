<?php

namespace App\Modules\Accounting\Retencion\Domain\Types;

use MyCLabs\Enum\Enum;

/**
 * @method static FormaRetencion RETENCION()
 * @method static FormaRetencion EMPRESA()
 */
final class FormaRetencion extends Enum
{
    private const RETENCION = 1;

    private const EMPRESA = 2;
}
