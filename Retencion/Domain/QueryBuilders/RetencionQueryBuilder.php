<?php

namespace App\Modules\Accounting\Retencion\Domain\QueryBuilders;

use Illuminate\Database\Eloquent\Builder;

class RetencionQueryBuilder extends Builder
{
    public function deleteFromOrigen(string $origenType, int $origenId)
    {
        $this->origen($origenType, $origenId)->delete();
    }

    public function deleteLogicFromOrigen(string $origenType, int $origenId)
    {
        $this->origen($origenType, $origenId)->update(['estado' => 0]);
    }

    private function origen(string $origenType, int $origenId)
    {
        return $this->where('origen_type', $origenType)->where('origen_id', $origenId);
    }
}
