<?php

namespace App\Modules\Accounting\Retencion\Application\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Accounting\Retencion\Domain\Services\GetRetencionReportForImpuestosService;
use App\Util\BaseResponse;
use Illuminate\Http\Request;

class GetRetencionReportForImpuestosController extends Controller
{
    /**
     * @var GetRetencionReportForImpuestosService
     */
    private $service;

    public function __construct(GetRetencionReportForImpuestosService $service)
    {
        $this->service = $service;
    }

    public function __invoke(Request $request)
    {
        $response = new BaseResponse();

        $year = $request->anio;
        $month = $request->mes;
        $sucursalId = $request->sucursal_id ?? 0;

        $response->data = $this->service->execute($sucursalId, $year, $month);

        return response()->json($response);
    }
}
