<?php

namespace App\Modules\Accounting\Retencion\Application\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Accounting\Configuration\Domain\Services\GetParametrosContablesRetencionService;
use App\Modules\Accounting\Retencion\Domain\Repositories\RetencionRepository;
use App\Util\BaseResponse;

class RetencionCreateController extends Controller
{
    /**
     * @var GetParametrosContablesRetencionService
     */
    private $parametrosContablesRetencionService;
    /**
     * @var RetencionRepository
     */
    private $repository;

    public function __construct(GetParametrosContablesRetencionService $parametrosContablesRetencionService,
        RetencionRepository $repository)
    {
        $this->parametrosContablesRetencionService = $parametrosContablesRetencionService;
        $this->repository = $repository;
    }

    public function __invoke()
    {
        $response = new BaseResponse();

        $response->data = [
            'parametros' => $this->parametrosContablesRetencionService->execute(),
            'tipos_retencion' => $this->repository->getTiposRetencion(),
        ];

        return response()->json($response);
    }
}
